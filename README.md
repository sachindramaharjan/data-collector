Docker Commands:

#Build
docker build -t data-collector:latest .

#Run
docker run --name data-collection-container
-d -v /Users/sachindra.maharjan/projects/data-collector/log:/var/log/Application/
-p 8080:8080 data-collector:latest default

#Push
docker tag data-collector sachindramaharjan/data-collector:.0.0.1
docker push sachindra/data-collector:0.0.1

#Docker compose

#run
docker-compose up

#detached mode
docker-compose up -d

#build and run
docker-compose up --build

