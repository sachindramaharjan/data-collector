# Base Alpine Linux based image with OpenJDK JRE only
FROM dtnapp/jdk-8-alpine-maven

MAINTAINER sachindra.maharjan4@gmail.com

# ----
# Install project dependencies and keep sources
# make source folder
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
# install maven dependency packages (keep in image)
COPY pom.xml /usr/src/app

# copy other source files (keep in image)
COPY src /usr/src/app/src

RUN mvn -T 1C clean install

# copy jar file from target folder
RUN ["cp", "/usr/src/app/target/data-collector.jar", "/data-collector.jar"]

#Remove the compiled files
RUN rm -rf target

COPY logback.xml /logback.xml

COPY data-collector.sh /usr/src/app/data-collector.sh

RUN ["chmod", "+x", "/usr/src/app/data-collector.sh"]

ENTRYPOINT ["/usr/src/app/data-collector.sh"]